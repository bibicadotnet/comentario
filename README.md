# Comentario

[Homepage](https://comentario.app/) · [Live Demo](https://demo.comentario.app) · [Docs](https://docs.comentario.app/) · [Author's Blog](https://yktoo.com/)

---

**[Comentario](https://comentario.app)** is a platform that you can embed in your website to allow your readers to add comments. It's lightweight and fast.

Available features:

* **Privacy by design**: no tracking scripts, ads etc.
* Multiple **login options**:
    * Local auth (with email and password)
    * Social: via Google, Twitter/X, Facebook, GitHub, GitLab
    * Single Sign-On (including non-interactive)
    * Anonymous comments
* Comment **replies** and nested comments
* **Markdown formatting**, supporting links, images, tables etc.
* **Sticky** comments
* Comment **editing** and **deletion**
* **Voting** on a comment
* Custom **user avatars**: uploaded or from Gravatar
* **Email notifications** about replies or comments pending moderation
* **Multiple domains** in a single Administration UI
* Flexible **moderation** settings
* External comment **content checkers** (extensions)
* View and comment **statistics**, per-domain and overall
* Import from Disqus, WordPress, Commento.

## FAQ

### How is this different from Disqus, Facebook Comments, and the rest?

Most other products in this category have privacy downsides you should know about.

Many of these systems like to collect data — stuff like analytics, ads, and user info. This can be a privacy issue, especially if they're after your personal data.

Apart from turning you, the user, into a product, theres a data security concern. If the commenting system gets hacked, your info might end up in the wrong hands. A good idea is to check the privacy policies and how they handle data before picking a commenting system for your site.

Here's a start:

* Comentario has no ads: you're the customer, not the product.
* Comentario offers self-hosted options that give you more control over privacy settings.
* Comentario is orders of magnitude lighter than alternatives: the downloadable embed part is some 20 KB compressed.

### Why should I care about my readers' privacy?

Firstly, it fosters trust and bolsters your website's reputation. When users believe that their personal information is handled responsibly, they are more likely to engage with your content and feel comfortable interacting with your online platform. Additionally, compliance with privacy laws and regulations is a must, as failure to do so can lead to legal repercussions and fines, making it essential to prioritize privacy as a fundamental aspect of your online presence.

Moreover, beyond legal requirements, there are ethical considerations. Respecting user privacy reflects a commitment to treating your readers with respect and acknowledging the value of their personal data. This ethical stance not only aligns with responsible online practices but also contributes to long-term sustainability by fostering user loyalty and trust. By prioritizing user privacy, you create a secure and welcoming digital space, which is vital for your website's credibility, user engagement, and overall success.

### How does Comentario differ from its predecessor Commento?

The list of differences is [really long](CHANGELOG.md), but here are a few major points:

* Comentario is in active development, regularly adding tons of features and improvements.
* Comentario is running the latest and greatest software versions, with all necessary security updates.
* Comentario is blazing fast due to extensive code and data model optimisations.
* Comentario is built following the best practices for security, privacy, responsive design, and accessibility.
* Comentario is (aiming to be) fully tested using automated tests to prevent regressions.

## Getting started

Refer to [Comentario documentation](https://docs.comentario.app/en/getting-started/) to learn how to install and configure it.
