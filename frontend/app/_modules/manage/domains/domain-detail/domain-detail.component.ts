import { Component } from '@angular/core';

@Component({
    selector: 'app-domain-detail',
    template: '<router-outlet></router-outlet>',
})
export class DomainDetailComponent {}
