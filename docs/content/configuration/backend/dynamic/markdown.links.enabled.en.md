---
title: Enable links in comments
description: markdown.links.enabled
tags:
    - configuration
    - dynamic configuration
    - administration
---

This [dynamic configuration](/configuration/backend/dynamic) parameter configures whether links can be inserted in comments.

<!--more-->

If set to `Off`, commenters won't be able to insert [links](/kb/markdown#links) in comments. Plain-text URLs won't be turned into clickable links either. Only applies to newly written comments.
