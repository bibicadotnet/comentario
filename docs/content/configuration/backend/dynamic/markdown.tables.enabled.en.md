---
title: Enable tables in comments
description: markdown.tables.enabled
tags:
    - configuration
    - dynamic configuration
    - administration
---

This [dynamic configuration](/configuration/backend/dynamic) parameter configures whether tables can be inserted in comments.

<!--more-->

If set to `Off`, commenters won't be able to insert [tables](/kb/markdown#tables) in comments. Only applies to newly written comments.
