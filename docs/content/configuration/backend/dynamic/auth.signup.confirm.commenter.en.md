---
title: New commenters must confirm their email
description: auth.signup.confirm.commenter
tags:
    - configuration
    - dynamic configuration
    - administration
---

This [dynamic configuration](/configuration/backend/dynamic) parameter configures whether users registering on comment pages are required to confirm their email before they can log in.

<!--more-->

If set to `Off`, users can log in immediately upon registration (not recommended due to security considerations).
