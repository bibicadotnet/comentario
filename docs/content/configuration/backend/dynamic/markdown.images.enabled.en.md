---
title: Enable images in comments
description: markdown.images.enabled
tags:
    - configuration
    - dynamic configuration
    - administration
---

This [dynamic configuration](/configuration/backend/dynamic) parameter configures whether images can be inserted in comments.

<!--more-->

If set to `Off`, commenters won't be able to insert [images](/kb/markdown#images) in comments. Only applies to newly written comments.
