---
title: Enable registration of new users
description: auth.signup.enabled
tags:
    - configuration
    - dynamic configuration
    - administration
---

This [dynamic configuration](/configuration/backend/dynamic) parameter controls whether new users are allowed to register in Comentario.

<!--more-->

If set to `Off`, no new user can register (applies both to embedded comments and the Administration UI). Can be useful for (temporarily) preventing new sign-ups.
