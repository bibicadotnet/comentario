---
title: comentario.app
layout: front-page
---

## What is Comentario?

Comentario is an open-source web comment engine, which adds discussion functionality to plain, boring web pages.

Just put a `<comentario-comments>` where you want a comment thread to appear, and you're all set!

{{< button "/configuration/embedding" "Learn More" "outline-secondary" "BO" >}}

## Why Comentario?

{{< div "row" >}}

{{< div "col-md-4" >}}
### Free & Open

Comentario is a free, [open-source](/about/source-code) software. Anyone can download or self-host it.
{{< /div >}}

{{< div "col-md-4" >}}
### Tiny

The embeddable script is only **20 KB** gzipped. That's orders of magnitude less than Disqus or Facebook Comments.
{{< /div >}}

{{< div "col-md-4" >}}
### Privacy-Focused

Comentario doesn't add any tracking scripts or pixels to your pages. And serves no ads!
{{< /div >}}

{{< div "col-md-4" >}}
### Powerful

Comentario is packed with [features](/about/features): it supports nested comments, Markdown, social login, and more.
{{< /div >}}

{{< div "col-md-4" >}}
### Clean & Friendly

Configure [moderation policy](/configuration/frontend/domain/moderation) per domain, including using [external services](/configuration/frontend/domain/extensions) (such as Perspective API) to detect toxic comments.
{{< /div >}}

{{< div "col-md-4" >}}
### Responsive

Comentario supports all screen sizes, both for embedded comments and the Administration UI.
{{< /div >}}

{{< /div >}}

{{< button "/about/features" "More about Features" "secondary" "BO" >}}


## See It in Action

Visit our Live Demo site to witness it for yourself!

{{< button "https://demo.comentario.app/" "Comments Demo" "primary mb-2" "BO" >}}
{{< button "https://edge.comentario.app/en/auth/login" "Administration UI" "outline-primary mb-2" "BO" >}}

Use the email `admin@admin` and password `admin` to log in.


## Get Started

Please refer to the [](/getting-started) documentation to learn how to get Comentario up and running.

{{< button "/getting-started" "Show Me the Docs!" "cta" "BO" >}}
