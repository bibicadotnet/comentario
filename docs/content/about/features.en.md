---
title: Features
description: What's so special about Comentario?
weight: 10
tags:
    - about
    - features
---

## Features in a nutshell

* **Privacy by design**: no tracking scripts, ads etc.
* Multiple **login options**:
    * Local auth (with email and password)
    * Social: via Google, Twitter/X, Facebook, GitHub, GitLab
    * Single Sign-On (including non-interactive)
    * Anonymous comments
* Comment **replies** and nested comments
* **Markdown formatting**, supporting links, images, tables etc.
* **Sticky** comments
* Comment **editing** and **deletion**
* **Voting** on a comment
* Custom **user avatars**: uploaded or from Gravatar
* **Email notifications** about replies or comments pending moderation
* **Multiple domains** in a single Administration UI
* Flexible **moderation** settings
* External comment **content checkers** (extensions)
* View and comment **statistics**, per-domain and overall
* Import from Disqus, WordPress, Commento.

Let's now look into the available features in more detail.

From the end-user perspective, Comentario consists of two parts: the **embedded comment engine** and the **Administration UI** (we also call it the frontend). 

## Embedded comments

Comentario's embedded comment engine allows to render a [comment thread (tree)](/kb/comment-tree), and each page it's embedded on has its own comment tree.

* Comments can have children — which we call **replies**. Child comments can also be *collapsed* with a button.
* Comment text can be formatted using the [Markdown syntax](/kb/markdown): you can make words **bold**, insert images and links, and so on.
* Comment thread uses mobile-first responsive design, which adapts well to different screen sizes.
* Comments can be edited and deleted.
* Other users can vote on comments they like or dislike (unless [disabled](/configuration/backend/dynamic/domain.defaults.comments.enablevoting.en)). Voting is reflected in the comment **score**.

{{< imgfig "/img/comentario-embed-ui-elements.png" "Example of comment tree on a web page." "border shadow p-4" >}}

* There's a variety of login options available for commenters; there's also an [option](/configuration/frontend/domain/authentication) to write a comment anonymously, should the site owner enable it for this specific domain.
* Users can upload their own avatars, or opt to use [images from Gravatar](/configuration/backend/dynamic/domain.defaults.usegravatar.en).

## Administration UI

The Administration UI is an extensive web application that allows users to perform all kinds of administrative tasks, moderate comments, view statistics, etc.

There's also the Profile section, where users can edit their profile, change their password, upload a custom avatar (or use a Gravatar). It also allows users to [delete their account](/legal/account-removal).

The functionality available in the Administration UI depends on [user roles](/kb/permissions) and includes:

* Managing domain settings:
    * [Authentication](/configuration/frontend/domain/authentication) (anonymous comment, social login, [SSO](/configuration/frontend/domain/authentication/sso))
    * [Moderation](/configuration/frontend/domain/moderation) (which comments are to be queued for moderation)
    * [Extensions](/configuration/frontend/domain/extensions) capable of spam and toxicity detection
    * Domain user management
* Import from [Disqus](/installation/migration/disqus), [WordPress](/installation/migration/wordpress), [Commento](/installation/migration/commento)
* Comment moderation
* Email notifications
* View and comment statistics
* Various domain operations (data export, comment removal, freezing, etc)
